# GRU+SVM for MNIST Classification #

This is an implementation of my proposed [GRU+SVM architecture](https://www.researchgate.net/publication/317016806_A_Neural_Network_Architecture_Combining_Gated_Recurrent_Unit_GRU_and_Support_Vector_Machine_SVM_for_Intrusion_Detection), but in this repository, it is intended for MNIST classification.

The MNIST (Modified National Institute of Standards and Technology) dataset is a dataset of hand-written digits, commonly used for training image processing systems.

## Abstract

Gated Recurrent Unit (GRU) is a recently published variant of the Long Short-Term Memory (LSTM) network, designed to solve the vanishing gradient and exploding gradient problems. However, its main objective is to solve the long-term dependency problem in Recurrent Neural Networks (RNNs), which prevents the network to connect an information from previous iteration with the current iteration. This study proposes a modification on the GRU model, having Support Vector Machine (SVM) as its classifier instead of the Softmax function. The classifier is responsible for the output of a network in a classification problem. SVM was chosen over Softmax for its efficiency in learning. The proposed model will then be used for intrusion detection, with the dataset from Kyoto University's honeypot system in 2013 which will serve as both its training and testing data.

## Requirements

* Linux-based OS (e.g. Ubuntu, Mint, Fedora)
* Python 3.5
* TensorFlow (1.3.0, GPU version recommended)

## Usage

For first run, execute the shell script `gru_svm.sh`. For succeeding runs, just type `python3 gru_svm_mnist.py` in the terminal.

## Sample Training Result

With the pre-defined hyper-parameters of the model, the following is a sample training result of the GRU+SVM model on MNIST classification.

```
Epoch : 0 completed out of 10, loss : 183.0938720703125, accuracy : 0.87109375
Epoch : 1 completed out of 10, loss : 83.48553466796875, accuracy : 0.953125
Epoch : 2 completed out of 10, loss : 96.95372009277344, accuracy : 0.94921875
Epoch : 3 completed out of 10, loss : 36.80517578125, accuracy : 0.984375
Epoch : 4 completed out of 10, loss : 57.431053161621094, accuracy : 0.97265625
Epoch : 5 completed out of 10, loss : 31.22872543334961, accuracy : 0.98046875
Epoch : 6 completed out of 10, loss : 33.04032516479492, accuracy : 0.98046875
Epoch : 7 completed out of 10, loss : 31.262266159057617, accuracy : 0.984375
Epoch : 8 completed out of 10, loss : 20.89887046813965, accuracy : 0.98828125
Epoch : 9 completed out of 10, loss : 26.805370330810547, accuracy : 0.98828125
Accuracy : 0.9761001467704773
```

The default hyper-parameters are as follows:

```
BATCH_SIZE = 256
CELL_SIZE = 256
DROPOUT_P_KEEP = 0.85
EPOCHS = 10
LEARNING_RATE = 1e-3
SVM_C = 1
```